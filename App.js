/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React ,{Component} from 'react';
import {} from 'react-native';
import Home                    from './src/telas/home'
import Filhos                  from './src/telas/filhos'
import Comunicados             from './src/telas/comunicados'
import CadastroResponsavel     from './src/telas/cadastroresponsavel'
import Camera                  from './src/telas/camera'
import Login                   from './src/telas/login'
import CodigoSms               from './src/telas/codigosms'
import { createAppContainer ,createSwitchNavigator} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
const StackPrincipal = createStackNavigator({
    Home:{
      screen:Home,
      navigationOptions:{
        header:null
      }
    },
    Filhos:{screen:Filhos},
    Comunicados:{screen:Comunicados},
    CadastroResponsavel:{screen:CadastroResponsavel},
    Camera:{screen:Camera}
},{
  initialRouteName:'Home',
  headerMode:'none'
})
const LoginCadastroStack = createStackNavigator({
  Login:{screen:Login},
  CodigoSms:{screen:CodigoSms}
},{
  initialRouteName:'Login',
  headerMode:'none'
})
const LoginStack = createSwitchNavigator({
  Home:{screen:StackPrincipal},
  Login:{screen:LoginCadastroStack}, 
},{
  initialRouteName:"Login", 
})
const AppNavigator = createAppContainer(LoginStack)
export default class App extends Component{
  constructor(props){
    super(props)
   
  }
  
  
  render(){
    return(
      <AppNavigator />
    )
  }
}