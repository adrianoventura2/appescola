import {  } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
const urlLogin             = 'http://appescola.aconos.com.br/oauth/token'
const urlUser              = 'http://appescola.aconos.com.br/api/user'
const urlFihos             = 'http://appescola.aconos.com.br/api/alunos'
const urlRequestSms        = 'http://appescola.aconos.com.br/api/users/requestSmsCode'
const urlConfirmarCadastro = 'http://appescola.aconos.com.br/api/users/confirmarCadastro'
const urlComunicados       = "http://appescola.aconos.com.br/api/comunicados"
function gravarAsyncStorage(nome,item){
    AsyncStorage.setItem(nome,item)
}
async function getItemAsyncStorage(nome){
    let item = await AsyncStorage.getItem(nome)
    return item
}
//'phone_number' => 'required|numeric|max:11|min:11',     // Número do telefone
async function getToken(telefone){
    let token = await getItemAsyncStorage('token');
    if(!token){
        token = await logar(telefone,telefone);
        if(token.token){
            token = token.token;
            return token;
        }else{
            return null;
        }
    }
    return token
}
async function getSms(telefone){
    let token = await getToken(telefone)
    if(!token) return {error:'Usuario não autorizado.'};
    console.log('USNADO O NUMERO',telefone)
    return fetch(urlRequestSms,{
        method:'POST',
        headers:{
            Accept:'application/json',
            'Content-Type':'application/json',
            'Authorization':'Bearer '+token
        },
        body:JSON.stringify({
            "phone_number":telefone
        })
    }).then((res)=>res.json())
      .then((res)=>{
          console.log('RESPOSTA DA FUNÇÃO GETSMS',res);
          return res
      })
      .catch((e)=>{
          console.log('ERROR NA FUNÇÃO GET SMS',e);
          return {error: 'Não foi possivel receber o codigo sms'}
      });
}
/**
 *  'image_file' => 'required|mimes:jpeg|max:3072',         // Imagem selfie do usuário
    'phone_number' => 'required|numeric|max:11|min:11',     // Número do telefone
    'player_id' => 'required',                              // ID único one signal
    'sms_code' => 'required|numeric|max:6|min:6'            // Código recebido por SMS mockup usar: (888888)
 */
async function confirmarCadastro(imagem,telefone,player_id,sms_code){    
    let token = await getToken(telefone)
    if(!token) return {error:'Usuario não autorizado.'};
    let formdata = new FormData();
    formdata.append('sms_code', sms_code)
    formdata.append('player_id', player_id)
    formdata.append('phone_number', telefone)
    formdata.append('image_file', {uri: imagem.uri, name: imagem.fileName, type: imagem.type})
    return fetch(urlConfirmarCadastro,{
        method:'POST',
        headers:{
            'Content-Type': 'multipart/form-data',
            'Authorization':'Bearer '+token
        },
        body:formdata
    }).then((res)=>res.json())
      .then((res)=>{
          console.log('RESPOSTA DA FUNÇÃO CONFIRMARCADASTRO',res);
          return res;
      })
      .catch((e)=>{
          console.log('ERROR NA FUNÇÃO CONFIRMARCADASTRO',e);
          return {error:'Não foi possivel Confirmar o cadastro.'};
      })
}
async function getComunicados(){
    let token = await getItemAsyncStorage('token');
    if(!token) return {error:'Não foi possivel pegar os comunicados'}
    return fetch(urlComunicados,{
        method:'GET',
        headers:{
            Accept:'application/json',
            'Content-Type':'application/json',
            'Authorization':'Bearer '+token
        }
    }).then((res)=>res.json())
      .then((res)=>{
          console.log('RESPOSTA DA FUNÇÃO GETCOMUNICADOS',res);
          return res
      })
      .catch((e)=>{
          console.log('ERRO NA FUNCAO GETCOMUNICADOS',e);
          return {error:'Não foi possivel pegar os comunicados.'}
      })
}
async function logar(username,password){    
    let token = '';
    let error = false;
    console.log('FAZENDO LOGIN COM O USERNAME',username,'PASSWORD',password)
    return fetch(urlLogin, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "grant_type": "password",
            "client_id": 2,
            "client_secret": "VYE9fwwckiKRVJKFmrxsCSdayAJp8aiMyKhMl7NC",
            "username": username,
            "password": password
        }),
        }).then((res)=>res.json())
          .then((res)=>{
             console.log('RESPOSTA DO SERVIDOR ESCOLA',res)

              if(res.access_token){
                  gravarAsyncStorage('token',res.access_token)
                  token = res.access_token;
                  return  {
                    token:token,
                    error:error
                }
              }else{
                return  {
                    token:'',
                    error:true
                }

              }
          }).catch(e=>{
            console.log('ERRO NA FUNÇÃO login ',e);
            return  {token,error:true};
          });
}
async function getUser(){
    let token = await getItemAsyncStorage('token')
    console.log('FAZENDO O FECH COM O TOKEN',token)
    return fetch(urlUser,{
        method:'GET',
        headers:{
            Accept:'application/json',
            'Content-Type':'application/json',
            'Authorization':'Bearer '+token
        }
    }).then((res)=>res.json())
      .then((res)=>{
          console.log('RESPOSTA COM INFO DO USER',res)
          return res
      })

}
async function getFilhos(){
    let token = await getItemAsyncStorage('token')
    console.log('FAZENDO O FECH COM O TOKEN',token)
    return fetch(urlFihos,{
        method:'GET',
        headers:{
            Accept:'application/json',
            'Content-Type':'application/json',
            'Authorization':'Bearer '+token
        }
    }).then((res)=>res.json())
      .then((res)=>{
          console.log('RESPOSTA COM INFO DO USER',res)
          return res
      })
}
export default api = {
    logar:logar,
    getUser:getUser,
    getFilhos:getFilhos,
    getSms:getSms,
    confirmarCadastro:confirmarCadastro,
    getComunicados:getComunicados
}