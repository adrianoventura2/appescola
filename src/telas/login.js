import React, { Component } from 'react';
import {View,TextInput,Alert, Text ,TouchableWithoutFeedback,ImageBackground ,ActivityIndicator} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import styles   from '../styles/styles'
import {Cores}  from '../styles/styles'
import api      from '../api/api'
import OneSignal from 'react-native-onesignal';
import AsyncStorage from '@react-native-community/async-storage';
import Botao        from '../componentes/botao'
const ONESIGNALID = '41785c50-f866-410c-b11f-81c7217994aa';


class Login extends  Component{
    constructor(props){
        super(props)
        this.Logar.bind(this)
        OneSignal.init(ONESIGNALID);       
        this.componentWillUnmount.bind(this)
        this.componentDidMount.bind(this)
        this.onReceived.bind(this)
        this.onOpened.bind(this)
        this.onIds.bind(this)
        this.state={
            numeroTelefone:'',
            numeroTelefonePuro:'',
            loading:false
        }
    }
    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('ids', this.onIds);
      }
    componentDidMount(){
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened.bind(this));
        OneSignal.addEventListener('ids', this.onIds);
    }
    
       onReceived(notification) {
        console.log("Notification received: ", notification);
        

      }
    
     async onOpened(openResult) {
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
        let login = await AsyncStorage.getItem('login')
        console.log('IRA SE LOGAR COM O LOGIN',login)
        if(login){
            let senha = await AsyncStorage.getItem('senha')
            let res   = await api.logar(login,senha);
            console.log('INDO NAVEGAR PARA COMUNICADOS',res)
            if(!res.error)  this.props.navigation.navigate('Comunicados');
        }
      }
    
      onIds(device) {
        console.log('Device info: ', device);
      }
   async Logar(){
        this.setState({loading:true})
        let senha = '123456'
        console.log('FUNCAO LOGIN ATIVADA', this.state.numeroTelefonePuro)
        //this.props.navigation.navigate('Home')
        let res = await  api.logar(this.state.numeroTelefonePuro,senha)
        if(!res.error){            
            AsyncStorage.setItem('login',this.state.numeroTelefonePuro);
            AsyncStorage.setItem('senha',senha);
            this.props.navigation.navigate('Home')
        }else{
            Alert.alert(
                'Erro',
                'Nao foi possivel logar!',
                [                  
                   {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: true},
              );
        }
        
    }
    render(){
        return(
            <ImageBackground source={require('../assets/imagemlogin.jpg')} 
                             resizeMethod='scale'
                             resizeMode='cover'
                             style={styles.containerLogin}>
                
                <View style={styles.containerNumero}>
                    <Text style={styles.textNumero}>Número do Celular</Text>
                    <TextInputMask
                        style={styles.inputNumero}
                        type={'cel-phone'}
                        options={{
                            maskType: 'BRL',
                            withDDD: true,
                            dddMask: '(99) '
                        }}
                        includeRawValueInChangeText={true}
                        value={this.state.numeroTelefone}
                        onChangeText={(text,rawText) => {
                            this.setState({
                            numeroTelefone: text,
                            numeroTelefonePuro:rawText
                            },()=>{
                                console.log('TAMANHO DO TEXT',this.state.numeroTelefone.length)
                                if(this.state.numeroTelefone.length === 15){
                                    this.Logar()
                                }
                            })
                        }}
                        />

                </View>
                    
               {
                   this.state.loading && (<View style={styles.loadingContainer} >                   
                                                <ActivityIndicator size={'large'} color={Cores.principal} />                                     
                                            </View>
                   )
               } 
               
               <Botao text={'Cadastrar-Se'} onPress={()=>this.props.navigation.navigate('CodigoSms')} />

            </ImageBackground>
        )
    }

}
export default Login