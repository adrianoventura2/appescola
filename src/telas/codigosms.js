import React, { Component } from 'react';
import {View,
        TextInput,
         Alert,Modal,
          Text ,
          TouchableWithoutFeedback,
          ImageBackground ,
          ActivityIndicator,
          Image,
          FlatList} from 'react-native'

import { TextInputMask } from 'react-native-masked-text'
import styles   from '../styles/styles'
import {Cores}  from '../styles/styles'
import ImagePicker from 'react-native-image-picker';
import api      from '../api/api'
import OneSignal from 'react-native-onesignal';
import CardFilho from '../componentes/cardfilho'
import Botao     from '../componentes/botao'



class CodigoSms extends  Component{
    constructor(props){
        super(props)
        this.confirmarSms.bind(this)
        this.getSms.bind(this)        
        this.state={
            codigoSms:'',
            codigoSmsPuro:'',
            loading:false,
            userId:'',
            loadingGetSms:false,
            numeroTelefone:'',
            numeroTelefonePuro:'',
            errorGetSms:'',
            filhos:[],
            confirmarFilhos:false,
            modalFilhos:false
        }
        this.componentDidMount.bind(this)
        this.componentWillUnmount.bind(this)
        this.tirarFotoEnviarCadastro.bind(this)
        this.onIds.bind(this)
    }
   async getSms(){
     this.setState({loadingGetSms:true,errorGetSms:''})
     let res = await api.getSms(this.state.numeroTelefonePuro)
     console.log('RESPOSTA DO GETSMS',res);
     if(res.error){
        this.setState({loadingGetSms:false,errorGetSms:res.error})
     }else{
       this.setState({loadingGetSms:false,errorGetSms:''})
     }
   }
   async tirarFotoEnviarCadastro(){
    const options = {
      title: 'Tire uma selfie',                
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options,async  (response) => {
      // Same code as in above section!
      console.log('Response = ', response,this.state.userId);
      
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };
       let res = await api.confirmarCadastro(response,this.state.numeroTelefonePuro,this.state.userId,this.state.codigoSmsPuro)
       console.log('RESPOSTA DO CADASTRO',res);
       if(res && (res === 'ok')){
        Alert.alert(
          'Atualização de cadastro.',
          'Feita com sucesso.',
          [                  
             {text: 'OK', onPress: () => this.props.navigation.navigate('Login')},
          ],
          {cancelable: true},
        );
       }

      }
      
    });
   }
   async confirmarSms(){
        this.setState({loading:true})
        console.log('FUNCAO GET  SMS', this.state.codigoSmsPuro)
        //this.props.navigation.navigate('Home')
      //  let res = await  api.confirmarSms(this.state.codigoSmsPuro,'123456')
      
        
          //Carrega informação do usuario
          let usuario = await api.getUser(this.state.numeroTelefonePuro);
          
         
          console.log('USUARIO',usuario);
          this.setState({
               loading:false,
               modalFilhos:true,
               filhos:usuario.alunos
          },()=>{
            

          })
          
      
      /**
       * if(!res.error){
            this.props.navigation.navigate('Home')
        }else{
            Alert.alert(
                'Erro',
                'Nao foi possivel confirmarSms!',
                [                  
                   {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: true},
              );
        }
       * 
       */
        
        
    }
    componentDidMount() {
       OneSignal.addEventListener('ids', this.onIds.bind(this));
      }
      
      componentWillUnmount() {
          OneSignal.removeEventListener('ids', this.onIds.bind(this));
      }
      
      onIds(device) {
          console.log('Device info: ', device);
          this.setState({
            userId:device.userId
          },()=>console.log('USER ID',this.state.userId)) 
          

      }
    render(){
        return(
            <ImageBackground source={require('../assets/imagemlogin.jpg')} 
                             resizeMethod='scale'
                             resizeMode='cover'
                             style={styles.containerLogin}>
                             <Modal onRequestClose={()=>{
                                      this.setState({modalFilhos:false});
                                    }} 
                              visible={this.state.modalFilhos}>
                                    <View style={styles.homeContainer}>
                                    <Image resizeMode='contain'
                                            style={styles.imagemlogoescola} 
                                            source={require('../assets/logoescola.png')} />
                                    <FlatList
                                        contentContainerStyle={styles.containerFilhos}
                                        data={this.state.filhos}
                                        renderItem={({ item }) => <CardFilho filho={item} />}
                                        keyExtractor={item => item.nome}
                                    />
                                    <View style={styles.containerBotaoConfirmarFilhos}>
                                        <Botao text={'Confirmar'} 
                                          onPress={()=>{
                                            this.setState({modalFilhos:false})
                                            this.tirarFotoEnviarCadastro()
                                            }}
                                        />

                                    </View>
                                    
                                    </View>
                             </Modal>
                <View style={styles.containerNumero}>
                    <Text style={styles.textNumero}>Número do Celular</Text>
                    <TextInputMask
                        style={styles.inputNumero}
                        type={'cel-phone'}
                        options={{
                            maskType: 'BRL',
                            withDDD: true,
                            dddMask: '(99) '
                        }}
                        includeRawValueInChangeText={true}
                        value={this.state.numeroTelefone}
                        onChangeText={(text,rawText) => {
                            this.setState({
                            numeroTelefone: text,
                            numeroTelefonePuro:rawText
                            },()=>{
                                console.log('TAMANHO DO TEXT',this.state.numeroTelefone.length)
                                if(this.state.numeroTelefone.length === 15){
                                    this.getSms()
                                }
                            })
                        }}
                        />

                </View>
                {
                   this.state.loadingGetSms && (<View style={styles.loadingContainer}>                   
                                                <Text style={styles.textNumero}>
                                                  Aguarde o codigo por sms
                                                  {this.state.errorGetSms}
                                                  </Text>
                                                <ActivityIndicator size={'large'} color={Cores.principal} />                                     
                                                </View>
                   )
               } 
               {
                 (this.state.errorGetSms !== '')&&(
                                                  <Text style={styles.textNumero}>                                                  
                                                  {this.state.errorGetSms}
                                                  </Text>
                 )
               }
                
                <View style={styles.containerNumero}>
                    <Text style={styles.textNumero}>Código Sms</Text>
                    <TextInputMask
                        style={styles.inputNumero}
                        type={'only-numbers'}
                        keyboardType='phone-pad'
                        options={{
                            mask:'999999'
                        }}
                        includeRawValueInChangeText={true}
                        value={this.state.codigoSms}
                        onChangeText={(text,rawText) => {
                            if(text.length > 6) return
                            this.setState({
                            codigoSms: text,
                            codigoSmsPuro:rawText
                            },()=>{
                                console.log('TAMANHO DO TEXT',this.state.codigoSms.length)
                                if(this.state.codigoSms.length === 6){
                                    this.confirmarSms()
                                }
                            })
                        }}
                        />

                </View>
                    
               {
                   this.state.loading && (<View style={styles.loadingContainer} >                   
                                                <ActivityIndicator size={'large'} color={Cores.principal} />                                     
                                            </View>
                   )
               } 

            </ImageBackground>
        )
    }

}
export default CodigoSms