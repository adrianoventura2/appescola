import React, { Component } from 'react';
import {View, Text ,Image, TouchableWithoutFeedback,FlatList,ActivityIndicator} from 'react-native'
import api from '../api/api'
import {Cores} from '../styles/styles'
import styles from '../styles/styles'
import AsyncStorage from '@react-native-community/async-storage';

var moment   = require('moment');
let ptMoment = require('moment/locale/pt-br')
moment.locale('pt-Br')
class Comunicados extends  Component{
    constructor(props){
        super(props)
        this.carregarComunicados.bind(this)
        this.componentDidMount.bind(this)
        this.apagarComunicado.bind(this)
        this.state = {
            comunicados:[],
            error:'',
            loading:true
        }

    }
    componentDidMount(){
        this.carregarComunicados()
    }
   async  carregarComunicados(){
        let comunicados = await api.getComunicados()
        console.log('COMUNICADOS NA VIEW',comunicados)
        if(comunicados.hasOwnProperty('error')) {
            this.setState({error:comunicados.error,loading:false});
            return
        }else{            
            let comunicadosApagados   = await AsyncStorage.getItem('comunicadosApagados')            ;
            if(!comunicadosApagados)    comunicadosApagados = ''
            //mantem só os q n foram apagados
            console.log('COMUNICADOS APAGADOS',comunicadosApagados);
            comunicados = comunicados.filter((comunicado)=> (comunicadosApagados.includes(comunicado.id)) === false);
            const comunicadosRecentes = comunicados.sort((a, b) => moment(b.timestamp).diff(moment(a.timestamp)))
            this.setState({comunicados:comunicadosRecentes,error:'',loading:false});
        }

    }
    async apagarComunicado(id){
        console.log('ESTA NA FUNCAO DE APAGAR O COMUNICADO',id)
       
        let comunicados = this.state.comunicados
        comunicados = comunicados.filter((comunicado)=>comunicado.id != id);
        //novos comunicados
        this.setState({comunicados},async ()=>{
            //salvar como apagado
            let comunicadosapagados = await AsyncStorage.getItem('comunicadosApagados')
            if(!comunicadosapagados) comunicadosapagados = ''
            comunicadosapagados+= ','+id;
            AsyncStorage.setItem('comunicadosApagados',comunicadosapagados);
        })
        

    }
    render(){
        return(
            <View style={styles.comunicadosContainer}  onPress={this.carregarComunicados}>
                 <Image resizeMode='contain'
                       style={styles.imagemlogoescola} 
                       source={require('../assets/logoescola.png')} />
              
               <Text style={styles.textTitulo}>Comunicados</Text>
               {
                   this.state.loading && (<View style={styles.loadingContainer} >                   
                                                <ActivityIndicator size={'large'} color={Cores.principal} />                                     
                                            </View>
                   )
               }
               <FlatList
                        data={this.state.comunicados}
                        
                        renderItem={({ item }) => (
                            <View style={styles.cardComunicado} key={item.id}>                        
                                <Text style={styles.textComunicado}>{item.text}</Text>
                            <View style={styles.containerDataComunicado}>
                                <Text>{moment(item.timestamp).format("dddd, MMMM Do YYYY, h:mm ")}</Text>
                                <TouchableWithoutFeedback onPress={()=>this.apagarComunicado(item.id)}>
                                    <Image style={styles.deleteIcone} source={require('../assets/icones/delete.png')}  />
                                </TouchableWithoutFeedback>                                
                                                         
                            </View>                            
                         </View>
                        )}
                        keyExtractor={item => item.id}
                       
                    />
              
               {(this.state.error !== '') && (<Text style={styles.textTitulo}>{this.state.error}</Text>)}
            </View>
        )
    }

}
export default Comunicados