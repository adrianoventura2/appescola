import React, { Component } from 'react';
import { View ,Image} from 'react-native'
import styles from '../styles/styles'
import Card from '../componentes/card'
class Home extends  Component{
    constructor(props){
        super(props)
        this.navegar.bind(this)
        this.state = {
            rotas:{
                filhos:{nome:'Filhos',navegaPara:'Filhos'},
                cadastrarResponsavel:{nome:'Cadastrar Novo Responsável',navegaPara:'CadastroResponsavel'},
                comunicados:{nome:'Comunicados',navegaPara:'Comunicados'}            
            }
        }
    }
    navegar(rota){
        console.log('FUNCAO LOGIN ATIVADA')
        this.props.navigation.navigate(rota)
    }
    render(){
        return(
            <View style={styles.homeContainer}>  
                <Image resizeMode='contain'
                       style={styles.imagemlogoescola} 
                       source={require('../assets/logoescola.png')} />
                <Card nome={this.state.rotas.filhos.nome} onPress={()=>this.navegar(this.state.rotas.filhos.navegaPara)}/>
                <Card nome={this.state.rotas.cadastrarResponsavel.nome} onPress={()=>this.navegar(this.state.rotas.cadastrarResponsavel.navegaPara)}/>
                <Card nome={this.state.rotas.comunicados.nome} onPress={()=>this.navegar(this.state.rotas.comunicados.navegaPara)}/>
            </View>
        )
    }

}
export default Home