import React, { Component } from 'react';
import { Text ,View,Image,ScrollView,TextInput,Keyboard,CheckBox,Picker} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import styles from '../styles/styles'
class CadastroResponsavel extends  Component{
    constructor(props){
        super(props)
        this.cadastrarResponsavel.bind(this)
        this.refNome;
        this.refParentesco;
        this.refTelefone;
        this.refVitalicio;
        this.refTemporario;

        this.state = {
            nome:'',
            parentesco:'',
            telefone:'',
            telefonePuro:'',
            vitalicio:false,
            temporario:'30',
            parentescos:[
                {nome:'Tio',valor:'tio'},
                {nome:'Tia',valor:'tia'},
                {nome:'Avô',valor:'avô'},
                {nome:'Avó',valor:'avó'},
                {nome:'Irmão',valor:'irmao'},
                {nome:'Irmã',valor:'irma'}
            ],
            tempos:[
                {nome:'30 dias',valor:'30'},
                {nome:'60 dias',valor:'60'},
                {nome:'90 dias',valor:'90'}
            ]
        }
    }
    cadastrarResponsavel(){
        console.log('FUNCAO LOGIN ATIVADA')
        this.props.navigation.navigate('Camera')
    }
    render(){
        return(
            <ScrollView contentContainerStyle={[styles.comunicadosContainer,{backgroundColor:'lightgray'}]}  onPress={this.carregarComunicados}>
                 <Image resizeMode='contain'
                       style={styles.imagemlogoescola} 
                       source={require('../assets/logoescola.png')} />              
               <Text style={styles.textTitulo}>Cadastrar Novo Responsável</Text>
               <View style={styles.formContainer}>                    
                   <View style={styles.formInputContainer}>
                        <View style={styles.viewInput}>
                            <Text>Nome</Text>
                        </View>
                        <View style={styles.viewInput}>
                           <Text>Parentesco</Text>
                        </View>
                        <View style={styles.viewInput}>
                            <Text>Telefone</Text>
                        </View>
                        <View style={styles.viewInput}>
                         <Text>Vitalicio</Text>
                        </View>
                        <View style={styles.viewInput}>                             
                             <Text>Temporario</Text>
                        </View>
                   </View>
                   <View style={styles.formInputContainer}>
                   <View style={styles.viewInput}>
                    <TextInput 
                       
                        ref={(ref)=>{this.refNome = ref}}
                        value={this.state.nome}
                        onSubmitEditing={(e)=>{
                            Keyboard.dismiss();
                            this.refParentesco.togglePicker()
                        }}
                        onChangeText={(text)=>this.setState({nome:text})}

                    />
                   </View>
                   <View style={styles.viewInput}>
                    <Picker
                            ref={(ref)=>{this.refParentesco = ref}}
                            selectedValue={this.state.parentesco}
                            style={{height: 50, width: 100}}
                            onValueChange={(itemValue, itemIndex) =>{
                                this.setState({parentesco: itemValue})
                                this.refTelefone.focus();
                                }
                            }>
                            <Picker.Item  label={''} value={''} />
                            {
                                this.state.parentescos.map((parente,index)=>(
                                    <Picker.Item key={parente.nome} label={parente.nome} value={parente.value} />
                                ))
                            }
                            
                            </Picker>

                   </View>
                   <View style={styles.viewInput}>
                    <TextInputMask
                            ref={(ref)=>{this.refTelefone = ref;}}
                            style={styles.inputNumero}
                            type={'cel-phone'}
                            options={{
                                maskType: 'BRL',
                                withDDD: true,
                                dddMask: '(99) '
                            }}
                            includeRawValueInChangeText={true}
                            value={this.state.telefone}
                            onChangeText={(text,rawText) => {
                                this.setState({
                                telefone: text,
                                telefonePuro:rawText
                                },()=>{
                                    console.log('TAMANHO DO TEXT',this.state.numeroTelefone.length)
                                    if(this.state.numeroTelefone.length === 15){
                                    Keyboard.dismiss()
                                    }
                                })
                            }}
                            />

                   </View>
                   <View style={styles.viewInput}>
                    <CheckBox 
                                value={this.state.vitalicio}
                                onValueChange={(value)=>{
                                    if(value === true){
                                        this.setState({vitalicio:value,parentesco:''})
                                    }else{
                                        this.setState({vitalicio:value})
                                    }
                                }}
                            />

                   </View>
                   <View style={styles.viewInput}>
                    <Picker
                            ref={(ref)=>{this.refTemporario = ref}}
                            selectedValue={this.state.temporario}
                            style={{height: 50, width: 100}}
                            onValueChange={(itemValue, itemIndex) =>{
                                if(this.state.vitalicio) return
                                this.setState({temporario: itemValue})
                                }
                            }>
                            <Picker.Item  label={''} value={''} />
                            {
                                
                                this.state.parentescos.map((parente,index)=>(
                                    <Picker.Item key={parente.nome} label={parente.nome} value={parente.value} />
                                ))
                            }
                            
                            </Picker>

                   </View>
                   
                   

                   
                       
                        

                   </View>
               </View>
               
            </ScrollView>
        )
    }

}
export default CadastroResponsavel