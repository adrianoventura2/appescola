import React, { Component } from 'react';
import {View, Text ,TouchableWithoutFeedback,ActivityIndicator ,FlatList,Image} from 'react-native'
import {Cores} from '../styles/styles'
import styles from '../styles/styles'
import api     from '../api/api'
import CardFilho from '../componentes/cardfilho'
class Filhos extends  Component{
    constructor(props){
        super(props)
        this.carregarFilhos.bind(this)
        this.componentDidMount.bind(this)
        this.state = {
            filhos:[],
            loading:true
        }
    }
    async componentDidMount(){
        let filhos = await api.getFilhos()
        if(filhos){
            console.log('RESPOSTA FILHOS',filhos)
            this.setState({
                filhos,
                loading:false
            })
        }
    }
    carregarFilhos(){
        console.log('FUNCAO LOGIN ATIVADA')
        this.props.navigation.navigate('CadastroResponsavel')
    }
    render(){
        return(
            <View style={styles.homeContainer}>    
              
              <Image resizeMode='contain'
                       style={styles.imagemlogoescola} 
                       source={require('../assets/logoescola.png')} />
               {
                   this.state.loading && (<View style={styles.loadingContainer} >                   
                                                <ActivityIndicator size={'large'} color={Cores.principal} />                                     
                                            </View>
                   )
               }
               <Text style={styles.textTitulo}>Filhos</Text>
               <FlatList
                    contentContainerStyle={styles.containerFilhos}
                    data={this.state.filhos}
                    renderItem={({ item }) => <CardFilho filho={item} />}
                    keyExtractor={item => item.nome}
                />
            </View>
        )
    }

}
export default Filhos