import React, { Component } from 'react';
import { Text ,TouchableWithoutFeedback} from 'react-native'
class Camera extends  Component{
    constructor(props){
        super(props)
        this.carregarCamera.bind(this)
    }
    carregarCamera(){
        console.log('FUNCAO LOGIN ATIVADA')
        this.props.navigation.navigate('Comunicados')
    }
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.carregarCamera.bind(this)}>
                <Text>Camera</Text>
            </TouchableWithoutFeedback>
        )
    }

}
export default Camera