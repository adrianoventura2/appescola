import React, { PureComponent } from 'react';
import { View,Text,TouchableWithoutFeedback } from 'react-native'
import styles from '../styles/styles'
export default class Botao extends PureComponent{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <TouchableWithoutFeedback    
                    style={styles.touchBotao}
                    onPress={()=>{
                        this.props.onPress()
                    }}
                    >
                    <View
                        style={styles.containerBotao}
                    >
                    <Text style={styles.textBotao}>{this.props.text}</Text>
                    </View>
                    
            </TouchableWithoutFeedback>

        )
    }
}