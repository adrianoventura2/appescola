import React, { PureComponent } from 'react'    
import { View ,Text, TouchableWithoutFeedback,Image} from 'react-native'
import styles from '../styles/styles'
class CardFilho extends PureComponent{
    constructor(props){
        super(props)
    }
    render(){
        const filho = this.props.filho
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                    <View style={styles.containerCardfilho}>
                        {(filho.profile_img_url && (filho.profile_img_url !== '')) ?(<Image style={styles.imageFilho} source={{uri:filho.profile_img_url}} resizeMode='contain' />) :(<Image style={styles.imageFilho} source={require('../assets/filho.jpg')} resizeMode='contain' />)  }
                        <View style={styles.containerTextCardFilho}>
                          <Text  style={styles.textcard}>{filho.nome}</Text>
                          <Text  style={styles.textcard}>{filho.serie} Série - Turma {filho.classe}</Text>
                          <Text  style={styles.textcard}>Turno {filho.turno}</Text>                          
                        </View>
                    </View>
            </TouchableWithoutFeedback>
         
        )
    }
}
export default CardFilho