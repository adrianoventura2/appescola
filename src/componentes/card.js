import React, { PureComponent } from 'react'    
import { View ,Text, TouchableWithoutFeedback,Image} from 'react-native'
import styles from '../styles/styles'
class Card extends PureComponent{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                    <View style={styles.containerCard}>
                        <Image style={styles.imageFilho} source={require('../assets/filho.jpg')} resizeMode='contain' />
                        <Text  style={styles.textcard}>{this.props.nome}</Text>
                    </View>
            </TouchableWithoutFeedback>
         
        )
    }
}
export default Card